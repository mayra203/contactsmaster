import React from 'react'

export default function NotFound() {
  return (
    <div>
      <h1 className="display-4"><span>404 Page not found</span></h1>
    </div>
  )
}
