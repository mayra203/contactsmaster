import React, { Component } from "react";
import { connect } from "react-redux";
import TextInputGroup from "../layout/TextInputGroup";
import PropTypes from 'prop-types';
import { addContact } from "../../actions/contactActions";
import uuid from "uuid";

class AddContact extends Component {
    state = {
        name: "",
        email: "",
        phone: "",
        errors: {}
    };
    onChange = e => this.setState({ [e.target.name]: e.target.value });
    onSubmit = (e) => {
        e.preventDefault();
        const { name, email, phone } = this.state;
        //Check
        if (name === "") {
            this.setState({ errors: { name: "Name is required" } });
            return;
        }
        if (email === "") {
            this.setState({ errors: { email: "Email is required" } });
            return;
        }
        if (phone === "") {
            this.setState({ errors: { phone: "Phone is required" } });
            return;
        }
        const newContact = { id: uuid(), name, email, phone };

        this.props.addContact(newContact)
        
        this.setState({
            name: "",
            email: "",
            phone: "",
            errors: {}
        });
        this.props.history.push('/')
    };
    render() {
        const { name, email, phone, errors } = this.state;
        return (
            <div className="card mb-3">
                <div className="card-header">Add Contact</div>
                <div className="card-body">
                    <form onSubmit={this.onSubmit}>
                        <TextInputGroup
                            label="Name"
                            name="name"
                            type="text"
                            placeholder="Enter Name..."
                            value={name}
                            onChange={this.onChange}
                            error={errors.name}
                        />
                        <TextInputGroup
                            label="Email"
                            name="email"
                            type="email"
                            placeholder="Enter Email..."
                            value={email}
                            onChange={this.onChange}
                            error={errors.email}
                        />
                        <TextInputGroup
                            label="Phone"
                            name="phone"
                            type="text"
                            placeholder="Enter Phone..."
                            value={phone}
                            onChange={this.onChange}
                            error={errors.phone}
                        />
                        <input
                            className="btn btn-block btn-light"
                            type="submit"
                            value="Add Contact"
                        />
                    </form>
                </div>
            </div>
        );
    }
}

AddContact.propTypes={
    addContact: PropTypes.func.isRequired,
}

export default connect(null,{addContact})(AddContact);
