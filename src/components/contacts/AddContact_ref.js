import React, { Component } from 'react'

class AddContact extends Component {
    constructor(props){
        super(props)

        this.nameInput= React.createRef()
        this.emailInput= React.createRef()
        this.phoneInput= React.createRef()
    }
    onChange=(e)=>this.setState({[e.target.name]: e.target.defaultValue})
    onSubmit=(e)=>{
        e.preventDefault()
        const contact={
            name: this.nameInput.current.value,
            email: this.emailInput.current.value,
            phone: this.phoneInput.current.value
        }
        console.log(contact)
    }
    static defaultProps = {
        name: "Fred",
        email: "fred@yha.com",
        phone: "777-777-7777"
    }
    render () {
        const {name,email,phone} = this.props
        return (
            <div className="card mb-3">
                <div className="card-header">Add Contact</div>
                <div className="card-body">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">                            
                            <label htmlFor="name" className="col-sm-2"></label>
                            <input placeholder="Enter Name..." name="name" className="form-control form-control-lg" type="text" defaultValue={name} ref={this.nameInput}/>
                        </div>
                        <div className="form-group">                            
                            <label htmlFor="email" className="col-sm-2"></label>
                            <input placeholder="Enter Email..." name="email" className="form-control form-control-lg" type="email" defaultValue={email} ref={this.emailInput}/>
                        </div>
                        <div className="form-group">                            
                            <label htmlFor="phone" className="col-sm-2"></label>
                            <input placeholder="Enter Phone..." name="phone" className="form-control form-control-lg" type="text" defaultValue={phone} ref={this.phoneInput}/>
                        </div>
                        <input className="btn btn-block btn-light" type="submit" value="Add Contact" />
                    </form>
                </div>
            </div>
        )
    }
}

export default AddContact