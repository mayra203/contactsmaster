import { GET_CONTACTS, DELETE_CONTACT, ADD_CONTACT } from '../actions/types'
const initialState = {
    contacts: [
        {
            id: 1,
            name: "Leanne Graham",
            email: "Sincere@april.biz",
            phone: "1-770-736-8031 x56442"
        },
        {
            id: 2,
            name: "Ervin Howell",
            email: "Shanna@melissa.tv",
            phone: "010-692-6593 x09125"
        },
        {
            id: 3,
            name: "Clementine Bauch",
            email: "Nathan@yesenia.net",
            phone: "1-463-123-4447"
        }
    ]
}

export default function( state=initialState, action ){
    switch(action.type){
        case GET_CONTACTS : 
            return {...state}
        case DELETE_CONTACT:
            return{
                ...state,
                contacts: state.contacts.filter(contact=> contact.id!==action.payload)
            }
        case ADD_CONTACT: 
            return{
                ...state,
                contacts: [action.payload, ...state.contacts]
            }
        default: 
            return state;
    }
}

