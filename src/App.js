import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Contacts from "./components/contacts/Contacts";
import Header from "./components/layout/Header";
import { Provider } from "react-redux";

import AddContact from "./components/contacts/AddContact";
import EditContact from "./components/contacts/EditContact";
import About from './components/pages/About';
import NotFound from './components/pages/NotFound';

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import store from './store';

function App() {
    return (
        <Provider store={store}>
            <Router>
                <div className="App">
                    <Header branding="Manager" />
                    <div className="container">
                        <Switch>
                            <Route exact path="/" component={Contacts}></Route>
                            <Route exact path="/contact/add" component={AddContact}></Route>
                            <Route exact path="/contact/edit/:id" component={EditContact}></Route>
                            <Route exact path="/about" component={About}></Route>
                            <Route component={NotFound}></Route>
                        </Switch>
                    </div>
                </div>
            </Router>
        </Provider>
    );
}

export default App;